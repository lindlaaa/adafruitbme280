from datetime import datetime
from Adafruit_BME280 import *
import requests
import time
import json
import sys

sensor = BME280(t_mode=BME280_OSAMPLE_8, p_mode=BME280_OSAMPLE_8, h_mode=BME280_OSAMPLE_8)
API_ENDPOINT = "https://environent-boot.herokuapp.com/measurements"

class Measurement:
    def __init__(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.time = datetime.now().isoformat()
        self.location = 'livingroom'
print('Starting temperature sensor perpetually...')
while True:
  degrees = sensor.read_temperature()
  pascals = sensor.read_pressure()
  hectopascals = pascals / 100
  humidity = sensor.read_humidity()

  measurement = json.dumps(Measurement(degrees, hectopascals,
      humidity).__dict__)

  try:
    r = requests.post(url = API_ENDPOINT, data = measurement)
    print('API: {}').format(r)
  except Exception as inst:
    print("Exception occured ================================")
    print(type(inst))    # the exception instance
    print(inst.args)     # arguments stored in .args
    print(inst)
    print('Exception ========================================')
  time.sleep(300)
